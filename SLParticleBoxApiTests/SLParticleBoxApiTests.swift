//
//  SLParticleBoxApiTests.swift
//  SLParticleBoxApiTests
//
//  Created by Stuart Levine on 4/13/18.
//  Copyright © 2018 Wildcat Productions. All rights reserved.
//

import XCTest
@testable import SLParticleBoxApi

class SLParticleBoxApiTests: XCTestCase {
    let jsonEncoder = JSONEncoder()
    let jsonDecoder = JSONDecoder()
    var boxDocument: BoxDocument?
    let testJSON = "{\"key\": \"temperature\",\"value\": \"25\",\"scope\": \"device\",\"device_id\": \"250000000000000000000001\", \"product_id\": 1234, \"updated_at\": \"2016-08-29T09:12:33.001Z\"}"

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testBoxDocument() {
        // Test decoding from json to BoxDocument object
        let jsonData = testJSON.data(using: .utf8, allowLossyConversion: false)
        XCTAssertNotNil(jsonData)

        if let jsonData = jsonData, let dict = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any], let jsonDict = dict
 {
            let boxDocument = try? jsonDecoder.decode(BoxDocument.self, from: jsonData)
            XCTAssertNotNil(boxDocument)
            self.boxDocument = boxDocument
            XCTAssertEqual(boxDocument?.key, jsonDict["key"] as? String)
            XCTAssertEqual(boxDocument?.value, jsonDict["value"] as? String)
            XCTAssertEqual(boxDocument?.scope, jsonDict["scope"] as? String)
            XCTAssertEqual(boxDocument?.deviceId, jsonDict["device_id"] as? String)
            XCTAssertEqual(boxDocument?.productId, jsonDict["product_id"] as? Int)
            XCTAssertEqual(boxDocument?.updatedAtString, jsonDict["updated_at"] as? String)
        }
        else {
            XCTFail("Expected data and converted dictionary")
        }

        // Test encoder
        if let boxDocument = self.boxDocument, let json = try? jsonEncoder.encode(boxDocument), let dict = try? JSONSerialization.jsonObject(with: json, options: []) as? [String: Any], let jsonDict = dict {
            XCTAssertEqual(boxDocument.key, jsonDict["key"] as? String)
            XCTAssertEqual(boxDocument.value, jsonDict["value"] as? String)
            XCTAssertEqual(boxDocument.scope, jsonDict["scope"] as? String)
            XCTAssertEqual(boxDocument.deviceId, jsonDict["device_id"] as? String)
            XCTAssertEqual(boxDocument.productId, jsonDict["product_id"] as? Int)
            XCTAssertEqual(boxDocument.updatedAtString, jsonDict["updated_at"] as? String)
        }
        else {
            XCTFail("Expected data and converted dictionary")
        }
    }
        
}
