//
//  SLParticleBoxApi.h
//  SLParticleBoxApi
//
//  Created by Stuart Levine on 4/13/18.
//  Copyright © 2018 Wildcat Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SLParticleBoxApi.
FOUNDATION_EXPORT double SLParticleBoxApiVersionNumber;

//! Project version string for SLParticleBoxApi.
FOUNDATION_EXPORT const unsigned char SLParticleBoxApiVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SLParticleBoxApi/PublicHeader.h>


