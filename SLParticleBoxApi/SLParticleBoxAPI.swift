//
//  SLParticleBoxAPI.swift
//  SLParticleBoxApi
//
//  Created by Stuart Levine on 4/14/18.
//  Copyright © 2018 Wildcat Productions. All rights reserved.
//
//  A Particle Box SDK/API implementation
//  API Source URL: https://app.swaggerhub.com/apis/particle-iot/box/0.1#/
//

import Foundation

public enum HTTPMethod: String {
    case get = "GET"
    case put = "PUT"
    case post = "POST"
    case delete = "DELETE"

    var string: String {
        return self.rawValue
    }
}

// Our model object for the API
// Conforms to the Codable protocol allowing for convertion
// from JSON to a native object and vice versa. Similar to
// NSCoding in objective-c

public class BoxDocument : Codable, CustomStringConvertible {
    // Codable vars
    public var key: String?
    public var value: String?
    public var scope: String?
    public var deviceId: String?
    public var productId: Int?
    public var updatedAtString: String?

    // Getters

    // returns Date object representation of the updatedAtString
    public var updatedAt: Date? {
        guard let updatedAtString = self.updatedAtString else { return nil }

        return dateFormatter.date(from: updatedAtString)
    }

    // Use for Display purposes
    public var longDateString: String {
        guard let updatedAt = self.updatedAt else { return "" }

        return self.longDateFormatter.string(from: updatedAt)
    }

    // API Date Formatter
    public let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return dateFormatter
    }()

    // Display Date Formatter
    public let longDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateStyle = .long
        return dateFormatter
    }()


    public enum CodingKeys: String, CodingKey {
        case key
        case value
        case scope
        case deviceId = "device_id"
        case productId = "product_id"
        case updatedAtString = "updated_at"
    }

    public init() {
        updatedAtString = dateFormatter.string(from: Date())
    }

    init?(coder aDecoder: Decoder) {
        guard let values = try? aDecoder.container(keyedBy: CodingKeys.self) else { return }

        self.key = try? values.decode(String.self, forKey: .key)
        self.value = try? values.decode(String.self, forKey: .value)
        self.scope = try? values.decode(String.self, forKey: .scope)
        self.deviceId = try? values.decode(String.self, forKey: .deviceId)
        self.productId = try? values.decode(Int.self, forKey: .productId)
        self.updatedAtString = try? values.decode(String.self, forKey: .updatedAtString)
    }

    func encode(with aCoder: Encoder) throws {
        var encoder = aCoder.container(keyedBy: CodingKeys.self)
        try encoder.encode(key, forKey: .key)
        try encoder.encode(value, forKey: .value)
        try encoder.encode(scope, forKey: .scope)
        try encoder.encode(deviceId, forKey: .deviceId)
        try encoder.encode(productId, forKey: .productId)
        try encoder.encode(updatedAtString, forKey: .updatedAtString)
    }

    public func reset() {
        self.key = nil
        self.value = nil
        self.scope = nil
        self.deviceId = nil
        self.productId = nil
        self.updatedAtString = nil
    }

    public var description: String {
        return "\n<\(type(of: self))>\n  key = \(key ?? "nil")\n  value = \(value ?? "nil")\n  scope = \(scope ?? "")\n  deviceId = \(deviceId ?? "nil")\n  productId = \(String(describing: productId))\n  updatedAt = \(String(describing: updatedAt))\n\n"
    }
}

// Response ENUM covers success and error cases
public enum SLPBResponse {
    case success([BoxDocument]?)
    case error(Error?)
}

public typealias SLPBCompletion = (SLPBResponse)->()

// Main SDK Class
public class SLParticleBoxSDK {
    public static let jsonEncoder = JSONEncoder()
    public static let jsonDecoder = JSONDecoder()
    private static let sessionConfig = URLSessionConfiguration.default
    private static let session = URLSession(configuration: sessionConfig)

    private static var apiKey = ""

    // enum to keep our scope property limited to the 3 permitted strings
    public enum PBScope: String {
        case product
        case user
        case device

        var string: String {
            return self.rawValue
        }
    }

    // Properties for each endpoint
    public struct PBEndpoint {
        var urlString: String
        var httpMethod: HTTPMethod
        var url: URL?
        var headers: [String: String]
        var body: Data?

        // Override the default constructor.  If we wanted to use both, this would be in an extension
        init(url: String, httpMethod: HTTPMethod, headers: [String: String] = [:], body: Data? = nil) {
            self.urlString = url
            self.httpMethod = httpMethod
            self.url = URL(string: urlString)
            self.headers = headers
            self.body = body
        }
    }

    // Filter object when searching the API backend using specific criteria outline in the API docs
    public struct PBFilter {
        var scope: PBScope?
        var deviceId: String?
        var productId: String?
        var filter: String?
        var page: Int?
        var perPage: Int?

        public init(scope: PBScope? = nil, deviceId: String? = nil, productId: String? = nil, filter: String? = nil, page: Int? = nil, perPage: Int? = nil) {
            self.scope = scope
            self.deviceId = deviceId
            self.productId = productId
            self.filter = filter
            self.page = page
            self.perPage = perPage
        }

        public var parameters: String {
            var parameters = ""
            if let scope = scope {
                let delimiter = parameters.count > 0 ? "&" : "?"
                parameters += "\(delimiter)scope=\(scope.string)"
            }
            if let deviceId = deviceId {
                let delimiter = parameters.count > 0 ? "&" : "?"
                parameters += "\(delimiter)device_id=\(deviceId)"
            }
            if let productId = productId {
                let delimiter = parameters.count > 0 ? "&" : "?"
                parameters += "\(delimiter)product_id=\(productId)"
            }
            if let filter = filter {
                let delimiter = parameters.count > 0 ? "&" : "?"
                parameters += "\(delimiter)filter=\(filter)"
            }
            if let page = page {
                let delimiter = parameters.count > 0 ? "&" : "?"
                parameters += "\(delimiter)page=\(page)"
            }
            if let perPage = perPage {
                let delimiter = parameters.count > 0 ? "&" : "?"
                parameters += "\(delimiter)per_page=\(perPage)"
            }
            return parameters
        }
    }

    // SDK Router
    public enum PBRoute {
        case list(PBFilter)
        case get(key: String, PBFilter)
        case create(BoxDocument)
        case delete(key: String)

        private static var baseURL = "https://virtserver.swaggerhub.com/particle-iot/box/0.1"

        static public func setBaseURL(_ url: String) {
            PBRoute.baseURL = url
        }

        // API Endpoint var - a getter based on the current route case
        public var endpoint: PBEndpoint {
            let headers = [
                "Authorization" : "Bearer \(SLParticleBoxSDK.apiKey)",
                "Content-Type" : "application/json",
                "Accept" : "application/json"
            ]

            switch self {
            case .list(let filter):
                let listURL = "\(PBRoute.baseURL)/box\(filter.parameters)"
                return PBEndpoint(url: listURL, httpMethod: .get, headers: headers)
            case .get(let key, let filter):
                let getURL = "\(PBRoute.baseURL)/box/\(key)\(filter.parameters)"
                return PBEndpoint(url: getURL, httpMethod: .get, headers: headers)
            case .create(let boxDocument):
                let jsonData = try? jsonEncoder.encode(boxDocument)
                return PBEndpoint(url: "\(PBRoute.baseURL)/box", httpMethod: .post, headers: headers, body: jsonData)
            case .delete(let key):
                return PBEndpoint(url: "\(PBRoute.baseURL)/box/\(key)", httpMethod: .delete, headers: headers)
            }
        }
    }

    // public configure method used to set the API key
    public static func configure(withApiKey apiKey: String) {
        SLParticleBoxSDK.apiKey = apiKey
    }

    // Generates an Error object with the provided code and message
    //
    // @param code: Int - the error code
    // @param message: String - the localizedDescription of the Error object
    //
    // @result Error object
    //
    private static func generateError(code: Int, message: String) -> Error {
        let infoDict = [NSLocalizedDescriptionKey : message]
        let error = NSError(domain: "com.particlebox.error", code: code, userInfo: infoDict)
        return error
    }

    // The main API calling function
    //
    // @param route: PPRoute - provids the enum describing the API Route and Endpoint to use
    // @param onCompletion: SLPBComletion - the optional completion block to execute when the async
    //                                      data tasks has completed
    //
    // @result URLSessionDataTask - allows more control over the data task
    //
    @discardableResult public static func apiCall(with route: PBRoute, onCompletion: SLPBCompletion?) -> URLSessionDataTask? {
        guard let url = route.endpoint.url else {
            let error = generateError(code: 404, message: "Invalid URL found.")
            onCompletion?(SLPBResponse.error(error))
            return nil
        }

        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 30)
        request.allHTTPHeaderFields = route.endpoint.headers
        request.httpMethod = route.endpoint.httpMethod.string

        switch route.endpoint.httpMethod {
        case .get: break
        case .put: break
        case .post:
            request.httpBody = route.endpoint.body
        case .delete: break
        }

        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                onCompletion?(SLPBResponse.error(error))
            }
            else if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                var docs = [BoxDocument]()
                if let json = json, let array = json["data"] as? [[String: Any]] {
                    for dict in array {
                        if let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: []), let doc = try? jsonDecoder.decode(BoxDocument.self, from: jsonData) {
                            docs.append(doc)
                        }
                    }
                }
                else if let json = json {
                    if let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []), let doc = try? jsonDecoder.decode(BoxDocument.self, from: jsonData) {
                        docs.append(doc)
                    }
                }

                onCompletion?(SLPBResponse.success(docs))
            }
            else {
                onCompletion?(SLPBResponse.success(nil))
            }
        }

        dataTask.resume()

        return dataTask
    }
}
